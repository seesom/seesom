/* ============================== 
 *	Functions adapted to seESOM   
 *	2015-10-20					  
 * ============================== */


// TITLE BAR
// =============================

var nw = require('nw.gui');
var win = nw.Window.get();
win.isMaximized = false;

// Close window
function Close()
{
	win.close(); 
};
        
// Minimize window
function Min()
{
	win.minimize();
};

// Maximize window
function Max()
{
	if (win.isMaximized)
		win.unmaximize();
	else
		win.maximize();
};

//keep track if window is max
win.on('maximize', function(){
	win.isMaximized = true;
});

//keep track if window is unmax
win.on('unmaximize', function(){
    win.isMaximized = false;
});


// TOOL BAR
// =============================

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function(){
$('.my-dropdown').dropdown();
});

$(document).ready(function(){
$('.my-dropdown').tooltip({trigger:"hover"});
});


// PARSE DATA
// =============================

//parse density file
function parseDensityFile(evt) {
    var file = evt.target.files[0];
    
    Papa.parse(file, {
      header: true,
      dynamicTyping: true,
      complete: function(results) {
        densitydata = results;
        initialize();
      }
    });
}

//parse contig file
function parseContigFile(evt) {
    var file = evt.target.files[0];
    
    Papa.parse(file, {
      header: true,
      dynamicTyping: true,
      complete: function(results) {
        contigdata = results;
        drawContig(1);
      }
    });
}
  
//read density file  
$(document).ready(function(){
    $("#densityLoader").change(parseDensityFile);
});

//read contig file  
$(document).ready(function(){
    $("#contigLoader").change(parseContigFile);
});


// PLOT DATA
// =============================

//draw contigs
function drawContig(scale) {
	for (var i in contigdata.data) {
		context.beginPath();
    	contig = contigdata.data[i];
    	context.globalAlpha = contig.density;
    	context.fillStyle = contig.colour;
    	context.arc(contig.x, contig.y, 1, 0, 2 * Math.PI, false);
    	context.closePath();
    	context.fill();
    }	
}

//draw background density
function draw(scale, cscale){
    // set canvas
    canvas.setAttribute("width" , canvas.width*cscale);
	canvas.setAttribute("height" , canvas.height*cscale);
    // clear canvas	
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.scale(scale, scale);
	context.fillStyle ='black';
    context.beginPath();
    for (var i in densitydata.data) {
        oRec = densitydata.data[i];
        context.globalAlpha = oRec.density;
        context.fillRect(oRec.x, oRec.y,1,1);
	}
    context.closePath();
}

//initialize drawing
function initialize(){
	// collect information on max for x and y
 	var maxy=0;
 	var maxx=0;
 	for (var i in densitydata.data) {
        oRec = densitydata.data[i];
        if (oRec.x > maxx)
        maxx = oRec.x;
        if (oRec.y > maxy)
        maxy = oRec.y;
    }
 	// set a canvas size
 	canvas.width = maxx;
	canvas.height = maxy;

 	// set scale variables
    var scale = 1.44;
    var scaleMultiplier = 1.2;
 
    //zoom in event
    document.getElementById("plus").addEventListener("click", function(){
        scale *= scaleMultiplier;
        draw(scale, scaleMultiplier);
        if (typeof contigdata !== 'undefined' && contigdata.data.length > 0) {
        	drawContig(scale);
        }
    }, false);
 
	//zoom out event 
    document.getElementById("minus").addEventListener("click", function(){
        scale /= scaleMultiplier;
        draw(scale, 1/scaleMultiplier);
        if (typeof contigdata !== 'undefined' && contigdata.data.length > 0) {
        	drawContig(scale);
        }
    }, false);

	//draw background density
    draw(scale, scale);
};


// SHOW INFO FOR DATA
// =============================

//OBS! This function is not complete and has error (problem with showing right contig)
function getTips(event) {
	
	//check if data exists
	if (typeof contigdata !== 'undefined' && contigdata.data.length > 0) {
	
		//create variables
        var x = new Number();
        var y = new Number();
		var hit = false;
		
		//get position
        if (event.x != undefined && event.y != undefined)
        {
          x = event.x;
          y = event.y;
        }
        else // Firefox method to get the position
        {
          x = event.clientX + document.body.scrollLeft +
              document.documentElement.scrollLeft;
          y = event.clientY + document.body.scrollTop +
              document.documentElement.scrollTop;
        }
		
		//set final position
        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;
		
		//search for contig
		for (var i in contigdata.data) {
        	if (contigdata.data[i].x === x && contigdata.data[i].y === y) {
        		tipCanvas.style.left = (contigdata.data[i].x - 50) + "px";
        		tipCanvas.style.top = (contigdata.data[i].y) + "px";
        		//tipCanvas.style.position = "absolute"; 
        		tipContext.clearRect(0, 0, tipCanvas.width, tipCanvas.height);
        		tipContext.fillText(contigdata.data[i].scaffold, 5, 15);
        		hit = true;
        		break;
			}
      	}      
        if (!hit) {
        tipCanvas.style.left = "-200px";
    	}
    }
}

